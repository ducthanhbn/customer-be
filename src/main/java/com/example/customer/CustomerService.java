package com.example.customer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CustomerService {
    private static List<CustomerEntity> list = new ArrayList<>();

    public List<CustomerEntity> loadAll(){
        log.info("get all list customer");
        return list;
    }

    public CustomerEntity getByCode(String code){
        log.info("get by code {}", code);
        return list.stream().filter(el -> el.getId().equals(code)).findFirst().orElse(new CustomerEntity());
    }

    public Boolean save(CustomerEntity customer){
        if(customer.getId() != null){
            log.info("update customer");
            list = list.stream().map(el -> {
                if(el.getId().equals(customer.getId())){
                    el.setEmail(customer.getEmail());
                    el.setPhone(customer.getPhone());
                    el.setName(customer.getName());
                }
                return el;
            }).collect(Collectors.toList());

        }
        else{
            log.info("save customer");
            customer.setId(randomString());
            try{
                list.add(customer);
            }
            catch (Exception e){
                log.error(e.getMessage(), e);
                return false;
            }
        }
        return true;
    }

    public Boolean delete(String code){
        log.info("delete by code {}", code);
        list = list.stream().filter(el -> !el.getId().equals(code)).collect(Collectors.toList());
        return true;
    }

    private String randomString(){
        log.info("random string");
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        String generatedString = buffer.toString();
        return generatedString;

    }

}

