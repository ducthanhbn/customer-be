package com.example.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/Customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping
    public List<CustomerEntity> getAll(){
        return customerService.loadAll();
    }

    @GetMapping("{code}")
    public CustomerEntity get(@PathVariable("code")String code){
        return customerService.getByCode(code);
    }

    @DeleteMapping("{code}")
    public Boolean delete(@PathVariable("code")String code){
        return customerService.delete(code);
    }

    @PostMapping
    public Map post(@RequestBody CustomerEntity customer){
        customerService.save(customer);
        Map tmp = new HashMap(); tmp.put("message", "added");
        return tmp;
    }
}
