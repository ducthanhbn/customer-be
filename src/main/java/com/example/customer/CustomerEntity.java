package com.example.customer;

import lombok.Data;

@Data
public class CustomerEntity {
    private String id;
    private String name;
    private String email;
    private String phone;
}
