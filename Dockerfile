FROM openjdk:21-jdk-slim-bullseye

COPY target/customer-0.0.1-SNAPSHOT.war /opt/app.war
CMD ["java", "-jar", "/opt/app.war"]